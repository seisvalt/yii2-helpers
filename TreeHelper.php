<?php

namespace seisvalt\helpers;

use yii\db\Expression;

class TreeHelper
{
    public static function addNode($father, $child)
    {
        $return = false;
        $child->left = $father->right;
        $child->right = $father->right + 1;
        $child->level = $father->level + 1;
        $child->parent = $father->id;
        $child->save();
        $child::updateAll(
            ['right' =>  new Expression('"right" + 2')],
            '"right" >= :left AND id != :id',
            ['left'=> $child->left, 'id' => $child->id]
        );
        $succes = $child::updateAll(
            ['left' =>  new Expression('"left" + 2')],
            '"left" > :left AND id != :id',
            ['left'=> $child->left, 'id' => $child->id]
        );

        if ($succes > 0) {
            $return = true;
        }

        return $return;

    }
}