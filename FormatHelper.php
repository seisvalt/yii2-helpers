<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 1/07/16
 * Time: 10:11 AM
 */

namespace seisvalt\helpers;


class FormatHelper
{
    public static function mac2Dec(string $mac)
    {
        $mac = strtolower($mac);
        $temp_mac = str_split($mac, 2);
        $mac=implode(":", $temp_mac);
        $int_mac = base_convert($mac,16,10);
        return $int_mac;
    }
}