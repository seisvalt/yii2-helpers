<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 30/03/15
 * Time: 04:53 PM
 */

namespace seisvalt\helpers;

use Yii;
use yii\helpers\BaseArrayHelper;

class ArrayHelper extends BaseArrayHelper
{
	private static $precision;

	public static function getPairs($array, $options, $pair=true, $keepKeys = false)
	{
		$key_pairs =$options['keys'];
		$llave = null;

		if(!$pair){
			$llave 	   = $key_pairs[0];
			$key_pairs = $key_pairs[1];
		}
		if ($keepKeys) {

			foreach ($array as $k => $element) {

				foreach($key_pairs as $columns){
					if($pair){
						$columna = $columns[1];
						$llave 	 = $columns[0];
					}else{
						$columna = $columns;
					}

					$arreglo=[$llave=> $element[$llave], $columna=> $element[$columna]];
					$result[$columna][]= $arreglo;
				}
			}

		} else {

			foreach ($array as $k => $element) {

				foreach($key_pairs as $columns){

					if($pair){
						$columna = $columns[1];
						$llave 	 = $columns[0];
					}else{
						$columna = $columns;
					}
					$arreglo=[ $element[$llave], $element[$columna]];
					$result[$columna][]= $arreglo;
				}

			}
		}
		return $result;
	}

	public static function getSelectValRound($array, $precision=0)
	{
		static::$precision = $precision;
		$retorno = array_map(
			function($data) {  $size = static::$precision; return("ROUND($data, $size) as $data"); },
			$array
		);
		return $retorno;
	}

	public static function mapValues($array, $text, $data)
	{
		$retorno=[];
		foreach($array as $value)
			$retorno[$value[$text]] =$data;
		return $retorno;
	}

  public static function getPoint($array, $key, $x, $y)
  {
    $retorno=[];
    foreach($array as $value)
        $retorno[$value[$key]][] = [strtotime($value[$x]." GMT -5") * 1000, $value[$y]];
    return $retorno;
  }

  public static function markActive(& $array, $route){
    foreach ($array as $i => &$menu) {
      if (!empty($menu['items']) && is_array($menu['items'])){
        self::markActive($menu['items'], $route);
      }
      if ($menu['url']) {
        $array[$i]['active'] = strpos($route, trim($menu['url'], '/')) === 0;
        //echo sprintf("\nlabel:%s , Active:%b \n",$menu['label'], $array[$i]['active']);
      }
    }
  }
}
